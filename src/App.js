import "./App.css";
import { BrowserRouter } from "react-router-dom";
import Routers from "./routers/Routers";
import { ShopProvider } from "./context/ShopContext";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <ShopProvider>
          <Routers />
        </ShopProvider>
      </BrowserRouter>
    </div>
  );
}

export default App;
