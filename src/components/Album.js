import React, { useContext, useState } from "react";
import AlbumContext from "../context/AlbumContext";
import Photos from "./Photos";
import "./Components.css";
import { Carousel, Pagination } from "react-bootstrap";
import slider_1 from "../assets/slider_1.jpg";
import slider_2 from "../assets/slider_2.jpg";

const x = 100;

const Album = () => {
  const { album } = useContext(AlbumContext);
  const [pag, setPag] = useState({from: 0, to: (x-1)});

  const handleNext = ()=>{
    let from = pag.to + 1;
    let to = pag.to + x;
    setPag({from, to});
  }

  const handlePrev = ()=>{
    let from = pag.from - x;
    let to = pag.from - 1;
    setPag({from, to});
  }

  return (
    <div>
      <Carousel>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src={slider_1}
            alt="First slide"
            height="500px"
            width="100vm"
          />
          <Carousel.Caption>
            <h3>First slide label</h3>
            <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src={slider_2}
            alt="Second slide"
            height="500px"
            width="100vm"
          />

          <Carousel.Caption>
            <h3>Second slide label</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
      <h2>NFT'S  Album</h2>
      <div className="container">
        {album.map((e, pos) =>{
          if(pos >= pag.from && pos <= pag.to){
            return(<Photos key={e.id} objPhoto={e} />)
          }
        }
        )}
      </div>
      
      <Pagination>
      <Pagination.Prev />
      <Pagination.Item>{1}</Pagination.Item>
      <Pagination.Item>{2}</Pagination.Item>
      <Pagination.Item>{3}</Pagination.Item>
      <Pagination.Next onClick={handleNext} />
      </Pagination>
    </div>
  );
};

export default Album;
