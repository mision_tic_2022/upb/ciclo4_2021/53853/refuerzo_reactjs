import React, { useContext } from "react";
import BlogContext from "../context/BlogContext";
import Post from "./Post";
import "./Components.css";

const Blog = () => {
  const { posts } = useContext(BlogContext);

  return (
    <div>
      <h2>Blog</h2>
      <div className="container">
        {posts.map((obj) => (
          <Post key={obj.id} post={obj} />
        ))}
      </div>
    </div>
  );
};

export default Blog;
