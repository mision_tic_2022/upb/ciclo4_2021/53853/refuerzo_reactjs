import React, { useContext } from "react";
import { Modal, Button, Table } from "react-bootstrap";
import ShopContext from "../context/ShopContext";

const CartModal = ({ show, handleClose }) => {
  const { cart } = useContext(ShopContext);

  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Modal heading</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Title</th>
              <th>Image</th>
            </tr>
          </thead>
          <tbody>
            {cart.map((e) => {
              return (
                <tr>
                  <td>{e.title}</td>
                  <td>
                      <img height="80px" width="80px" src={e.thumbnailUrl}/>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default CartModal;
