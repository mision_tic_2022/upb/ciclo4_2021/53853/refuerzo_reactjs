import React, { useContext } from "react";
import {Card, Button} from "react-bootstrap";
import ShopContext from "../context/ShopContext";

const Photos = ({objPhoto}) => {

  const {handleCart} = useContext(ShopContext);

  const handleAddCart = ()=>{
    handleCart(objPhoto);
  }

  return (
    <Card style={{ width: "18rem" }}>
      <Card.Img variant="top" src={objPhoto.thumbnailUrl} />
      <Card.Body>
        <Card.Title>{objPhoto.title}</Card.Title>
        <Button variant="success" onClick={handleAddCart}>Add cart</Button>
      </Card.Body>
    </Card>
  );
};

export default Photos;
