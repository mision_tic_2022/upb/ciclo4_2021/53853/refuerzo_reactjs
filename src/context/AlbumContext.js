import { createContext, useEffect, useState } from "react";
import { apiAlbum } from "./Api";

const AlbumContext = createContext();

const AlbumProvider = ({children})=>{

    const [album, setAlbum] = useState([]);

    useEffect(()=>{
        getAlbum();
    }, []);


    const getAlbum = ()=>{
        fetch(apiAlbum, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then( async (resp)=>{
            console.log(resp);
            let json = await resp.json();
            setAlbum(json);
        }).catch(error=>{
            console.error(error);
        }).finally();        
    }

    const data = {getAlbum, album};

    return <AlbumContext.Provider value={data}>{children}</AlbumContext.Provider>
}

export {AlbumProvider};
export default AlbumContext;