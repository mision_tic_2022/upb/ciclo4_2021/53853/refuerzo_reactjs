
const root = "https://jsonplaceholder.typicode.com"
const apiBlog = `${root}/posts`;
const apiAlbum = `${root}/photos`;

export {apiBlog, apiAlbum};