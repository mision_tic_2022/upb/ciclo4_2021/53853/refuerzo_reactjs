import { createContext, useEffect, useState } from "react";
import { apiBlog } from "./Api";

const BlogContext = createContext();

const BlogProvider = ({children})=>{

    const [posts, setPosts] = useState([]);

    useEffect(()=>{
        getPosts();
    }, []);

    const getPosts = ()=>{
        fetch(apiBlog).then(async (resp)=>{
            let json = await resp.json();
            setPosts(json);
        }).catch(error=>{
            console.error(error);
        }).finally();
    }

    const data = {getPosts, posts};

    return <BlogContext.Provider value={data}>{children}</BlogContext.Provider>
}

export {BlogProvider};
export default BlogContext;