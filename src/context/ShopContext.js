import { createContext, useState } from "react";


const ShopContext = createContext();


const ShopProvider = ({children})=>{

    const [cart, setCart] = useState([]);

    const handleCart = (photo)=> setCart([...cart, photo]);

    const data = {handleCart, cart};

    return <ShopContext.Provider value={data}>{children}</ShopContext.Provider>
}

export {ShopProvider};
export default ShopContext;