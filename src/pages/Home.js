import React, {useState} from "react";
import { Outlet } from "react-router";
import { AlbumProvider } from "../context/AlbumContext";
import { Navbar, Container, Nav } from "react-bootstrap";
import { Link } from "react-router-dom";
import { BlogProvider } from "../context/BlogContext";
import CartModal from "../components/CartModal";

const Home = () => {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <div>
      <Navbar bg="primary" variant="dark" expand="lg">
        <Container>
          <Navbar.Brand href="#home">Web 53853</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link as={Link} to="/" href="#blog">
                Blog
              </Nav.Link>
              <Nav.Link as={Link} to="album" href="#album">
                Album
              </Nav.Link>
              <Nav.Link href="#cart" onClick={handleShow}>
                Cart
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      {/******************************/}
      <AlbumProvider>
        <BlogProvider>
          <Outlet />
        </BlogProvider>
      </AlbumProvider>
      {/*************MODAL**********/}
      <CartModal show={show} handleClose={handleClose}/>
    </div>
  );
};

export default Home;
